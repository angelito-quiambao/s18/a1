
	//3
	let trainer = {
		// //4
		// name:"Ash Ketchum",
		// age: 10,
		// pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
		// friends: {
		// 	hoenn: ["May", "Max"],
		// 	kanto: ["Brock", "Misty"]
		// },
		// //5
		// talk: function(pkmnName){
		// 	console.log(`${pkmnName}! I choose you!`);
		// }
	}
	//4
	trainer.name = "Ash Ketchum";
	trainer.age = 10;
	trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
	trainer.friends = {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	};

	//5
	trainer.talk = function(pkmnName){
		console.log(`${pkmnName}! I choose you!`);
	}


	console.log(trainer);

	//6
	console.log(`Result of dot notation:`);
	console.log(trainer.name);

	//6
	console.log(`Result of square bracket notation:`);
	console.log(trainer["pokemon"]);

	//7
	console.log(`Result of talk method:`);
	trainer.talk(trainer.pokemon[0]);

	//8
	function Pokemon(name, lvl){
			this.name = name;
			this.level = lvl;
			this.health = lvl *2;
			this.attack = lvl;

			this.tackle = function(opponent){
				console.log(`${this.name} tackled ${opponent.name}`);

				//10
				opponent.health -= this.attack
				console.log(`${opponent.name}'s health is now reduced to ${opponent.health}`);

				//12
				if (opponent.health <= 0) {
					this.fainted(opponent);
					console.log(opponent);
				}
				else{
					console.log(opponent);
				}
			}

			this.fainted = function(opponent){
				console.log(`${opponent.name} fainted.`);
			}
		}

		//9
		let pikachu = new Pokemon("Pikachu", 12);
		console.log(pikachu);
		let geodude = new Pokemon("Geodude", 8);
		console.log(geodude);
		let mewtwo = new Pokemon("Mewtwo", 100);
		console.log(mewtwo);

		//13
		geodude.tackle(pikachu);
		mewtwo.tackle(geodude);



